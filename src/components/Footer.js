import React from "react";
import { Link } from "react-router-dom";
import { BsLinkedin, BsGithub, BsYoutube, BsInstagram } from "react-icons/bs";

const Footer = () => {
  return (
    <>
      {/* Footer 1st */}
      <footer className="py-4">
        <div className="container-xxl">
          <div className="row align-items-center">
            <div className="col-5">
              <div className="footer-top-data d-flex gap-30 align-items-center">
                <img src="images/newsletter.png" alt="newsletter" />
                <h2 className="mb-0 text-white">Sign Up for Newsletter</h2>
              </div>
            </div>
            <div className="col-7">
              <div class="input-group">
                <input
                  type="text"
                  className="form-control py-1"
                  placeholder="Your Email Address"
                  aria-label="Your Email Address"
                  aria-describedby="basic-addon2"
                />
                <span className="input-group-text p-2" id="basic-addon2">
                  Subscribe
                </span>
              </div>
            </div>
          </div>
        </div>
      </footer>

      {/* Footer 2nd */}
      <footer className="py-4">
        <div className="container-xxl">
          <div className="row">
            {/* Contact Us */}
            <div className="col-4">
              <h4 className="text-white mb-0">Contact Us</h4>
              <div>
                <address className="text-white fs-6 text-align-center py-3">
                  6211: Bakuryuha, Otaka, Japan <br /> Pincode: 2366
                </address>
                <a
                  href="tel:0912345678"
                  className="text-white d-block mt-3 mb-1"
                >
                  0912345678
                </a>
                <a
                  href="mailto:bakuryuha@gmail.com"
                  className="text-white d-block mt-2 mb-0"
                >
                  bakuryuha@gmail.com
                </a>

                <div className="social-icons text-white d-flex align-items-center gap-30 mt-4">
                  <a className="text-white" href="#">
                    <BsLinkedin className="fs-4" />
                  </a>
                  <a className="text-white" href="#">
                    <BsInstagram className="fs-4" />
                  </a>
                  <a className="text-white" href="#">
                    <BsGithub className="fs-4" />
                  </a>
                  <a className="text-white" href="#">
                    <BsYoutube className="fs-4" />
                  </a>
                </div>
              </div>
            </div>

            {/* Information */}
            <div className="col-3">
              <h4 className="text-white mb-0">Information</h4>
              <div className="footer-link d-flex flex-column">
                <Link className="text-white py-3 mb-1">Privacy Policy</Link>
                <Link className="text-white py-3 mb-1">Refund Policy</Link>
                <Link className="text-white py-3 mb-1">Shipping Policy</Link>
                <Link className="text-white py-3 mb-1">
                  Terms and Conditions
                </Link>
                <Link className="text-white py-3 mb-1">Blogs</Link>
              </div>
            </div>

            {/* Account */}
            <div className="col-3">
              <h4 className="text-white mb-0">Account</h4>
              <div className="footer-link d-flex flex-column">
                <Link className="text-white py-3 mb-1">About Us</Link>
                <Link className="text-white py-3 mb-1">Faq</Link>
                <Link className="text-white py-3 mb-1">Contact</Link>
              </div>
            </div>

            {/* Quick Links */}
            <div className="col-2">
              <h4 className="text-white mb-0">Quick Links</h4>
              <div className="footer-link d-flex flex-column">
                <Link className="text-white py-3 mb-1">Accessories</Link>
                <Link className="text-white py-3 mb-1">Laptops</Link>
                <Link className="text-white py-3 mb-1">Headphone</Link>
                <Link className="text-white py-3 mb-1">Smart Watches</Link>
                <Link className="text-white py-3 mb-1">Tablets</Link>
              </div>
            </div>
          </div>
        </div>
      </footer>

      {/* Footer 3rd */}
      <footer className="py-4">
        <div className="container-xxl">
          <div className="row">
            <div className="col-12">
              <p className="text-center text-white mb-0">
                &copy;{new Date().getFullYear()}: Powered by Developers Corner
              </p>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
