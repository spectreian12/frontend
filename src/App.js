import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Layout from "./components/Layout";
import Home from "./pages/Home";
import About from "./pages/About";
import Contact from "./pages/Contact";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            {/* Whatever pass here is a reflection from outlet in Layout.js */}

            {/* Home Page*/}
            <Route index element={<Home />} />
            {/* About Page*/}
            <Route path="/about" element={<About />} />
            {/* Contact Page*/}
            <Route path="/contact" element={<Contact />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
